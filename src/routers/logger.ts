import { Router } from 'express';
import LoggerProvider from '../config/logger';

const loggerRouter: Router = Router();

loggerRouter.use((_req, _res, next) => {
	LoggerProvider.logger = LoggerProvider.logger.child({
		requestId: Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)
	});

	next();
});

export default loggerRouter;
