import { ReplicationMode } from 'typeorm';
import { PostgresDriver } from 'typeorm/driver/postgres/PostgresDriver';
import { RLSConnection } from './RLSConnection';
import { RLSPostgresQueryRunner } from './RLSPostgresQueryRunner';
import { TenancyModelOptions } from '../../models/interfaces/tenantOptions';

export class RLSPostgresDriver extends PostgresDriver {
	private tenantId: number;

	constructor(
		connection: RLSConnection,
		tenancyModelOptions: TenancyModelOptions
	) {
		super(connection);
		Object.assign(this, connection.driver);

		this.tenantId = tenancyModelOptions.tenantId;
	}

	createQueryRunner(mode: ReplicationMode): RLSPostgresQueryRunner {
		return new RLSPostgresQueryRunner(this, mode, {
			tenantId: this.tenantId
		});
	}
}
