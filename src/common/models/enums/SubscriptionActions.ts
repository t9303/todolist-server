import { registerEnumType } from 'type-graphql';

export enum SubscriptionActions {
	CREATE = 'CREATE',
	UPDATE = 'UPDATE',
	DELETE = 'DELETE'
}

registerEnumType(SubscriptionActions, {
	name: 'SubscriptionActions'
});
