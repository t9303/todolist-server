import { Service } from 'typedi';
import { Resolver, Query } from 'type-graphql';
import Tenant from './entity';
import BaseResolver from '../BaseResolver';

@Service()
@Resolver(Tenant)
export default class TenantResolver extends BaseResolver {
	constructor() {
		super(Tenant);
	}

	@Query(() => [Tenant])
	async tenants(): Promise<Tenant[]> {
		this.logger.info(`fetching all tenants`);

		return await Tenant.find();
	}
}
